import { Component, OnInit } from '@angular/core';
import {UserService} from '../../_services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users = []
  state = {
    title: 'Add New Patient',
    processing: false,
    saving: false,
    errors: {
      name: '',
      age: '',
      address: '',
      phone: '',
      landline: ''
    }
  }
  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.userService.loadUsers()
      .subscribe(res => {
        this.users = res['data'];
      })
  }
  banUser(user, index) {
    if(user.processing) return;
    user.processing = true;
    this.userService.banUser(user)
      .subscribe(res=>{
        this.users[index].status = res['user'].status;
        user.processing = false;
      })
  }

  unbanUser(user, index) {
    if(user.processing) return;
    user.processing = true;
    this.userService.unbanUser(user)
      .subscribe(res=>{
        this.users[index].status = res['user'].status;
        user.processing = false;
      })
  }
}
