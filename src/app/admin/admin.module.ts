import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {routing} from './admin.routing';
import {FormsModule} from '@angular/forms';
import { NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { UserListComponent } from './user-list/user-list.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { TestHistoryComponent } from './test-history/test-history.component';
import { MedicationComponent } from './medication/medication.component';
import { DrugCompaniesComponent } from './drug-companies/drug-companies.component';
import { DrugsComponent } from './drugs/drugs.component';

@NgModule({
  declarations: [
    UserListComponent,
    PatientListComponent,
    TestHistoryComponent,
    MedicationComponent,
    DrugCompaniesComponent,
    DrugsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    NgbTooltipModule,
    routing
  ],
  exports: [
  ]
})
export class AdminModule { }
