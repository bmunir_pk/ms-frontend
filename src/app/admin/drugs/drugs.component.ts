import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {DrugCompanyService} from '../../_services/drug-company.service';
import {ToastrService} from 'ngx-toastr';
import {DrugsService} from '../../_services/drugs.service';

@Component({
  selector: 'app-drugs',
  templateUrl: './drugs.component.html',
  styleUrls: ['./drugs.component.css']
})
export class DrugsComponent implements OnInit {

  drugs = []
  companies = []
  imageSrc
  state = {
    savingDrug: false,
    title: 'Add New Patient',
    processing: false,
    saving: false,
    index: -1,
    errors: {
      title: "",
      description: "",
      drug_company_id: ""
    }
  }
  constructor(public authService: AuthenticationService,
              public drugService: DrugsService,
              public drugCompanyService: DrugCompanyService,
              private toastr: ToastrService
  ) {

  }

  ngOnInit(): void {

    this.drugCompanyService.getAllCompanies()
      .subscribe(res=>{
        this.companies = res['companies'];
      })
    this.loadDrugs();
  }

  loadDrugs () {
    this.drugService.getAllDrugs()
      .subscribe(
        res=>{
          this.drugs = res['drugs']
        },
        err=>{
          console.log(err)
        }
      )
  }

  saveDrug () {
    if(this.state.savingDrug) return;
    this.state.savingDrug = true;
    this.resetErrors();
    this.drugService.saveDrug(this.drugService.form)
      .subscribe(
        res=>{
          this.state.savingDrug = false;
          console.log(Number(this.drugService.form['id']),' == ',0,this.drugService.form['id'] == 0  )
          if(this.state.index<0) {
            this.drugs.push(res['drug'])
          } else {
            this.drugs[this.state.index] = res['drug']
          }
          this.state.index = -1
          document.getElementById('company_form_close').click()
          this.toastr.success('Company Has been saved', 'Success!')
        },
        err=>{
          this.state.savingDrug = false;
          this.state.errors = err.error.errors;
          this.toastr.error('something went wrong!', 'Error!')
        }
      )
  }

  deleteDrug (drug, index) {
    if(drug.processing) return;
    if(!confirm("Do you really want to delete this record?")) return;
    drug.processing=true;
    this.drugService.removeDrug(drug.id)
      .subscribe(
        res=>{
          drug.processing = false
          this.drugs.splice(index, 1);
          this.toastr.success(res['message'].content, res['message'].title)
        },
        err=> {
          drug.processing = false
          this.toastr.error(err.error.message.content, err.error.message.title);
        }
      )
  }

  resetErrors () {
    this.state.errors = {
      title: "",
      description: "",
      drug_company_id: ""
    }
  }

  resetDrugForm() {
    this.state.index = -1
    this.drugService.form = {
      title: "",
      description: "",
      drug_company_id: ""
    }
  }

  setEditForm (drug, index) {
    this.state.index = index;
    this.drugService.form = drug
  }

}
