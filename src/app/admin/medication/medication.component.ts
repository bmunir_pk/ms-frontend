import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {PatientService} from '../../_services/patient.service';
import {DrugsService} from '../../_services/drugs.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';
import {DateParser} from '../../_helpers/DateParser';

@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.css']
})
export class MedicationComponent implements OnInit {

  state = {
    selectedMedicineIndex: -1,
    selectedMedicationIndex: -1,
    processing: false,
    selectedMedication: {},
    savingMedication: false,
    medicineRow: {
      drug : {title: ''},
      drug_dosage: 1,
      in_morning: false,
      in_noon: false,
      in_evening: false,
      in_night: false,
      remarks: ''
    },
    addingMedicines: false,
    drugs: []
  }
  canceler;
  medicationForm = {
    remarks: '',
    status: '',
    medicines: []
  }
  patient_id;
  dateParser;
  constructor(public authService: AuthenticationService,
              public patientService: PatientService,
              public drugService: DrugsService,
              private toastr: ToastrService,
              private activatedRoute: ActivatedRoute) {
    this.dateParser = new DateParser();
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.patient_id = params['patient_id'];
      this.getPatient(params['patient_id']);
    });

  }
  getPatient (patientId) {
    if(this.state.processing) return;
    this.state.processing= true;
    this.patientService.getPatientDetail(patientId)
      .subscribe(res => {
          console.log(res);
          this.patientService.patientForm = res['patient']
          this.state.processing= false;

        },
        err=> {
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
          this.state.processing=false
        })
  }

  setMedicationToView (medication) {
    this.state.selectedMedication = medication;
  }

  setMedicationToEdit (medication, index) {
    this.state.selectedMedicationIndex = index;
    this.medicationForm = JSON.parse(JSON.stringify(medication));
    this.medicationForm.medicines = JSON.parse(JSON.stringify(medication.medication_details));
  }

  addNewRow () {
    this.medicationForm.medicines.push({
      drug: {title: '', id: 0},
      drug_dosage: 1,
      in_morning: false,
      in_noon: false,
      in_evening: false,
      in_night: false,
      remarks: ''
    })
  }
  removeDrug(dosage, index) {
    this.medicationForm.medicines.splice(index, 1);
  }
  setMedicineRow(dosage, index) {
    this.state.addingMedicines = true;
    this.state.medicineRow = dosage;
    this.state.selectedMedicineIndex = index;
  }
  saveMedication ( ) {
    console.log(this.medicationForm);
    //patient/{patient_id}/medication
    if(this.state.savingMedication) return;
    this.state.savingMedication = true;
    this.medicationForm
    this.patientService.saveMedication(this.medicationForm, this.patient_id)
      .subscribe(res => {
          console.log(res)
          this.state.savingMedication= false;
          document.getElementById('medication_form_close').click();
          if(Number(this.medicationForm['id']) > 0 ) {
            this.patientService.patientForm['medications'][this.state.selectedMedicationIndex] = res['data']
          } else {
            this.patientService.patientForm['medications'].push(res['data']);
          }
          this.state.selectedMedicationIndex = -1;
          if(res['message']) {
            this.toastr.success(res['message'].content, res['message'].title);
          }
        },
        err => {
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        })
  }

  searchDrug($event){
    this.state.drugs = [];
    if($event.target.value == '') return;
    if(this.canceler) {
      this.canceler.unsubscribe();
    }
    this.canceler = this.drugService.searchDrug($event.target.value)
      .subscribe(
        res=>{
          this.state.drugs = res['drugs'];
          if(res['message']) {
            this.toastr.success(res['message'].content, res['message'].title);
          }
        },
        err=>{
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        },
      );
  }
  selectDrug(drug, index){
    this.state.drugs = [];
    this.state.medicineRow.drug = drug
  }
  addMedicineRow(more) {
    if(this.state.selectedMedicineIndex<0) {
      this.medicationForm.medicines.push(this.state.medicineRow);
    } else {
      this.medicationForm.medicines[this.state.selectedMedicineIndex] = this.state.medicineRow;
    }
    this.state.addingMedicines = more;
    this.state.selectedMedicineIndex = -1;
    this.resetMedicineRow();
  }
  resetMedicineRow() {
    this.state.medicineRow = {
      drug : {title: ''},
      drug_dosage: 1,
      in_morning: false,
      in_noon: false,
      in_evening: false,
      in_night: false,
      remarks: ''
    }
  }
}
