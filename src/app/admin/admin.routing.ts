import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'
import {UserListComponent} from './user-list/user-list.component';
import {TestHistoryComponent} from './test-history/test-history.component';
import {PatientListComponent} from './patient-list/patient-list.component';
import {MedicationComponent} from './medication/medication.component';
import {DrugCompaniesComponent} from './drug-companies/drug-companies.component';
import {DrugsComponent} from './drugs/drugs.component';

export const routes: Routes = [
  { path: 'users-list', component: UserListComponent },
  { path: 'patients-list', component: PatientListComponent },
  { path: 'test-history', component: TestHistoryComponent },
  { path: 'test-history/:patient_id/patient', component: TestHistoryComponent },
  { path: 'medication/:patient_id/patient', component: MedicationComponent },
  { path: 'drug-companies', component: DrugCompaniesComponent },
  { path: 'drugs', component: DrugsComponent },
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes)
