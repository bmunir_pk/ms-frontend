import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {PatientService} from '../../_services/patient.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  patients = []
  state = {
    title: 'Add New Patient',
    processing: false,
    saving: false,
    errors: {
      name: '',
      age: '',
      address: '',
      phone: '',
      landline: ''
    }
  }
  constructor(public authService: AuthenticationService,
              public patientService: PatientService,
              private toastr: ToastrService
  ) {

  }

  ngOnInit(): void {
    this.loadPatients()
  }

  loadPatients () {
    this.patientService.getAllPatients()
      .subscribe(res => {
          this.patients = res['patients'];
          //console.log(this.patients);
        },
        err => {
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        })
  }

}
