import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugCompaniesComponent } from './drug-companies.component';

describe('DrugCompaniesComponent', () => {
  let component: DrugCompaniesComponent;
  let fixture: ComponentFixture<DrugCompaniesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugCompaniesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
