import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {PatientService} from '../../_services/patient.service';
import {ToastrService} from 'ngx-toastr';
import {DrugCompanyService} from '../../_services/drug-company.service';

@Component({
  selector: 'app-drug-companies',
  templateUrl: './drug-companies.component.html',
  styleUrls: ['./drug-companies.component.css']
})
export class DrugCompaniesComponent implements OnInit {
  companies = []
  imageSrc
  state = {
    savingCompany: false,
    title: 'Add New Patient',
    processing: false,
    saving: false,
    errors: {
      title: "",
      description: "",
      logo: "",
      website_url: "",
      phone_number: "",
      address: ""
    }
  }
  constructor(public authService: AuthenticationService,
              public drugCompService: DrugCompanyService,
              private toastr: ToastrService
  ) {

  }

  ngOnInit(): void {
    this.loadCompanies();
  }

  loadCompanies () {
    this.drugCompService.getAllCompanies()
      .subscribe(
        res=>{
          this.companies = res['companies']
        },
        err=>{
          console.log(err)
        }
      )
  }

  saveCompany () {
    if(this.state.savingCompany) return;
    this.state.savingCompany = true;
    this.resetErrors();
    this.drugCompService.companyForm.logo = this.imageSrc ? this.imageSrc : this.drugCompService.companyForm.logo;
    this.drugCompService.saveCompany(this.drugCompService.companyForm)
      .subscribe(
        res=>{
          this.state.savingCompany = false;
          console.log(Number(this.drugCompService.companyForm['id']),' == ',0,this.drugCompService.companyForm['id'] == 0  )
          if(typeof this.drugCompService.companyForm['id'] || Number(this.drugCompService.companyForm['id']) == 0 ) {
            this.companies.push(res['company'])
          }
          document.getElementById('company_form_close').click()
          this.toastr.success('Company Has been saved', 'Success!')
        },
        err=>{
          this.state.savingCompany = false;
          this.state.errors = err.error.errors;
          this.toastr.error('something went wrong!', 'Error!')
        }
      )
  }

  deleteCompany (company, index) {
    if(company.processing) return;
    if(!confirm("Do you really want to delete this record?")) return;
    company.processing=true;
    this.drugCompService.removeCompany(company.id)
      .subscribe(
        res=>{
          company.processing = false
          this.companies.splice(index, 1);
          this.toastr.success(res['message'].content, res['message'].title)
        },
        err=> {
          company.processing = false
          this.toastr.error(err.error.message.content, err.error.message.title);
        }
      )
  }
  resetErrors () {
    this.state.errors = {
      title: "",
      description: "",
      logo: "",
      website_url: "",
      phone_number: "",
      address: ""
    }
  }
  resetCompanyForm() {
    this.drugCompService.companyForm = {
      title: "",
      description: "",
      logo: "",
      website_url: "",
      phone_number: "",
      address: ""
    }
  }

  setEditForm (company) {
    this.drugCompService.companyForm = company
  }

  onFileChanged(event) {
    if(this.state.processing) return;
    this.state.processing = true;
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.state.processing = false
      };
      //upload Image and to test Service
    }
  }

}
