import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {HttpService} from '../../_services/http.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {UserService} from '../../_services/user.service';
import {PatientService} from '../../_services/patient.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.css']
})
export class AccountSettingsComponent implements OnInit {
  public form = {
    email: '',
    name: '',
    password: '',
    password_confirmation: '',
    is_doctor: 'no'
  };
  public passwordForm = {
    oldPassword: '',
    newPassword: '',
    newPassword_confirmation: '',
  };
  state = {
    isProcessing: false,
    isProcessingPassword: false,
    errorPasswordForm: {
      oldPassword: '',
      newPassword: '',
      newPassword_confirmation: '',
    },
    errors: {
      email: '',
      name: '',
      password: '',
      password_confirmation: '',
      is_doctor: ''
    }
  };
  public user;
  constructor(private authService: AuthenticationService,
              private http: HttpService,
              private userService: UserService,
              private toastr: ToastrService
  ) {
    this.user = this.authService.currentUserValue;
    this.form = this.user;
  }

  ngOnInit(): void {
  }

  updateProfile(){
    this.state.isProcessing = true;
    this.state.errors={
      email: '',
      name: '',
      password: '',
      password_confirmation: '',
      is_doctor: ''
    };
    this.userService.updateProfile( this.form)
      .subscribe(res => {
          console.log(res);
          this.state.isProcessing = false;
          this.user.name= this.form.name;
          this.user.is_doctor= this.form.is_doctor;
          this.authService.setUserInfo(this.user);
          this.toastr.success("your information has been updated", 'Success!')
          //trigger success modal
        },
        err => {
          this.state.isProcessing = false;
          this.toastr.error("Try Again!", 'Error!');
          this.state.errors = err.error.data;
        },
        ()=> {this.state.isProcessing = false;}
      );
  }

  updatepassword () {
    this.state.isProcessingPassword = true;
    this.state.errorPasswordForm={
      oldPassword: '',
      newPassword: '',
      newPassword_confirmation: ''
    };
    this.userService.updatePassword(this.passwordForm)
      .subscribe(res => {
          console.log(res);
          this.state.isProcessingPassword = false;
          this.passwordForm = {
            oldPassword: '',
            newPassword: '',
            newPassword_confirmation: ''
          }

          this.toastr.success("Password has been updated", 'Success!')
          //trigger success modal
        },
        err => {
          this.state.isProcessingPassword = false;
          this.toastr.error("Try Again!", 'Error!')
          this.state.errorPasswordForm = err.error.data;
        },
        () => {this.state.isProcessingPassword = false;}
      );
  }

}
