import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsDetailComponentComponent } from './patients-detail-component.component';

describe('PatientsDetailComponentComponent', () => {
  let component: PatientsDetailComponentComponent;
  let fixture: ComponentFixture<PatientsDetailComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsDetailComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
