import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'
import {PatientsComponent} from './patients.component';
import {MedicationComponent} from './medication/medication.component';
import {PatientsDetailComponentComponent} from './patients-detail-component/patients-detail-component.component';

export const routes: Routes = [
  { path: '', component: PatientsComponent },
  { path: ':patient_id/detail', component: PatientsDetailComponentComponent },
  { path: 'medication/:patient_id', component: PatientsDetailComponentComponent },
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes)
