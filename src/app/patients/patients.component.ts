import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../_services/authentication.service';
import {PatientService} from '../_services/patient.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  patients = []
  state = {
    title: 'Add New Patient',
    processing: false,
    saving: false,
    errors: {
      name: '',
      age: '',
      address: '',
      phone: '',
      landline: ''
    }
  }
  constructor(public authService: AuthenticationService,
              public patientService: PatientService,
              private toastr: ToastrService
  ) {

  }

  ngOnInit(): void {
    this.loadMyPatients()
  }

  loadMyPatients () {
    this.patientService.getAllPatients()
      .subscribe(res => {
        this.patients = res['patients'];
        //console.log(this.patients);
      },
        err => {
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        })
  }

  resetPatientForm() {
    this.patientService.patientForm = {
      name: "",
      age: "",
      address: "",
      phone: "",
      landline: "",
      user_id: 0
    }
  }
  setTitle(title){
    this.state.title = title;
  }

  setPatient(patient){
    this.patientService.patientForm = JSON.parse(JSON.stringify(patient));
  }

  savePatients () {
    if(this.state.saving) return
    this.state.saving = true;
    this.patientService.savePatient(this.patientService.patientForm)
      .subscribe(
        res => {
          this.state.saving = false;
          this.loadMyPatients();
          document.getElementById("close_model").click()
          if(res['message']) {
            this.toastr.success(res['message'].content, res['message'].title);
          }
        },
        err => {
          console.log(err);
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
          this.state.saving = false;
          this.state.errors= err.error.data

        }
      )
  }

  removePatient (patient, index) {
    if(!confirm("are you sure you want to remove this patient?")) return;
    if(patient.deleting) return
    patient.deleting = true;
    this.patientService.removePatient(patient.id)
      .subscribe(res => {
        patient.deleting = false;
        this.patients.splice(index,1);
          if(res['message']) {
            this.toastr.success(res['message'].content, res['message'].title);
          }
      },
        err => {
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        }
    )
  }



}
