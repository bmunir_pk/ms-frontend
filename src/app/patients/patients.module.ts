import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MedicationComponent} from './medication/medication.component';
import {PatientsComponent} from './patients.component';
import {routing} from './patients.routing';
import { PatientsDetailComponentComponent } from './patients-detail-component/patients-detail-component.component';
import {FormsModule} from '@angular/forms';
import { NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    MedicationComponent,
    PatientsComponent,
    PatientsDetailComponentComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    NgbTooltipModule,
    routing
  ],
  exports: [
    MedicationComponent,
    PatientsComponent
  ]
})
export class PatientsModule { }
