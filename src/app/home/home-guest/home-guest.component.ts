import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../_services/http.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-home-guest',
  templateUrl: './home-guest.component.html',
  styleUrls: ['./home-guest.component.css']
})
export class HomeGuestComponent implements OnInit {
  public form = {
    email: '',
    name: '',
    password: '',
    password_confirmation: '',
    is_doctor: 'no'
  };
  state = {
    isProcessing: false,
    errors: {
      email: '',
      name: '',
      password: '',
      password_confirmation: '',
      is_doctor: ''
    }
  };

  constructor(private http: HttpService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  signup(){
    this.state.isProcessing = true;
    this.state.errors={
      email: '',
      name: '',
      password: '',
      password_confirmation: '',
      is_doctor: ''
    };
    this.http._post('register', this.form)
      .subscribe(res => {
          console.log(res);
          this.state.isProcessing = false;
          this.form = {
            email: '',
            name: '',
            password: '',
            password_confirmation: '',
            is_doctor: 'no'
          }

          this.toastr.success(res['message'].content, res['message'].title);
          //trigger success modal
        },
        err => {
          this.state.isProcessing = false;
          if(err.error.message) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
          if(err.error.data) {
            this.state.errors = err.error.data;
          }
        }
      );
  }

}
