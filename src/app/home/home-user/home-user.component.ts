import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {PatientService} from '../../_services/patient.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.css']
})
export class HomeUserComponent implements OnInit {
  form = {
    patient_id: 0,
    test_result: '',
    name:'',
    image_path: '',
    remarks: ''
  }
  imageSrc;
  canceler;
  state = {
    processing: false,
    lookingForResults: true,
    resultLoaded: false,
    patients: [],
  }
  constructor(
    public authService: AuthenticationService,
    public patientService: PatientService,
    private toastr: ToastrService
  ) {
    if(this.authService.currentUserValue.is_doctor == 'no'){
      this.patientService.getPatientForUser(this.authService.currentUserValue.id)
        .subscribe(res=>{
          console.log(res)
          this.selectPatient(res['patient'])
        },
          error => { console.log(error)})
    }
  }

  ngOnInit(): void {
  }

  searchPatient($event){
    this.state.patients = [];
    if($event.target.value == '') return;
    if(this.canceler) {
      this.canceler.unsubscribe();
    }
    this.canceler = this.patientService.searchPatient($event.target.value)
      .subscribe(
        res=>{
          this.state.patients = res['patients'];
        },
        err=>{
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        },
      );
  }

  onFileChanged(event) {
    if(this.state.processing) return;
    this.state.processing = true;
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.state.processing = false
        this.testImage()
      };
      //upload Image and to test Service
    }
  }

  testImage (){
    if(this.state.processing) return;
    this.state.resultLoaded = false;
    this.state.processing = true;
    this.patientService.performTest({file: this.imageSrc})
      .subscribe(
        res => {
          this.imageSrc = '';
          this.state.processing = false;
          this.state.resultLoaded = true;
          this.form.image_path = res['image_file']
          this.form.test_result = res['result']
        },
        err=>{
          console.log(err)
          if(err.error['message']) {
            this.toastr.error(err.error['message'].content, err.error['message'].title);
          }
        },
        ()=>{this.state.processing = false;}
      )
  }

  resetForm () {
    if(this.authService.currentUserValue.is_doctor == 'no') {
      this.state.resultLoaded = false;
      this.form.test_result = '';
      this.form.remarks= '';
    }

    this.state.resultLoaded = false;
    this.form = {
      patient_id: 0,
      test_result: '',
      name:'',
      image_path: '',
      remarks: ''
    }
  }
  selectPatient(patient){
    this.state.patients = [];
    this.form.patient_id = patient.id;
    this.form.name = patient.name;
  }

  saveTestResult () {
    if(this.state.processing) return;
    this.state.processing = true
    this.patientService.saveTestResult(this.form)
      .subscribe(res=>{
          if(res['message']) {
            this.toastr.success("Test has been saved", 'Success');
          }
        this.state.processing = false
        this.resetForm();
      },
        err => {
          if(err.error['message']) {
            this.toastr.error("something wrong with input, pelase try again", 'Error!');
          }
          this.state.processing = false
        },
        ()=> {this.state.processing = false}
      )
  }


}
