import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from './config.service';
import {HttpService} from './http.service';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient, private _http: HttpService) { }

  getAll() {
    //return this.http.get<any[]>(`${config.apiUrl}/users`);
  }

  register(user) {
    return this._http._post(`register`, user);
  }

  updateProfile(userForm) {
    return this._http._post(`update-profile`, userForm);
  }

  updatePassword(userForm) {
    return this._http._post(`update-password`, userForm);
  }

  delete(id) {
    //return this.http.delete(`${config.apiUrl}/users/${id}`);
  }

  loadUsers() {
    return this._http._get(`admin/users/all`);
  }

  banUser(user) {
    return this._http._post(`admin/users/${user.id}/ban`, {user: user});
  }

  unbanUser(user) {
    return this._http._post(`admin/users/${user.id}/unban`, {user: user});
  }
}
