import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from './config.service';
import {HttpService} from './http.service';

@Injectable({ providedIn: 'root' })
export class PatientService {
  patientForm={
      name: "",
      age: "",
      address: "",
      phone: "",
      landline: "",
      user_id: 0
  }

  constructor(private http: HttpService) { }

  getAllPatients() {
    return this.http._get(`patient/all`);
  }

  getPatientDetail (patientId) {
    return this.http._get(`patient/${patientId}`);
  }

  getPatientForUser (userId) {
    return this.http._get(`patient/${userId}/user`);
  }

  searchPatient (term) {
    return this.http._get(`patient/search`, {term});
  }

  savePatient (patient) {
    return this.http._post(`patient`, patient);
  }

  removePatient (patientId) {
    return this.http._post(`patient/${patientId}`, {});
  }

  performTest (body) {
    return this.http._post(`covid-test/get-result`, body);
  }

  saveTestResult(body){
    return this.http._post(`covid-test/save`, body);
  }

  saveMedication (body, patientId) {
    return this.http._post(`patient/${patientId}/medication`, body)
  }

}
