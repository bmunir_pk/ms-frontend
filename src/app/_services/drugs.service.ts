import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from './config.service';
import {HttpService} from './http.service';

@Injectable({ providedIn: 'root' })
export class DrugsService {
  form= {
    title: "",
    description: "",
    drug_company_id: ""
  }

  constructor(private http: HttpService) { }

  searchDrug (term) {
    return this.http._get(`drugs/search`, {term});
  }

  getAllDrugs() {
    return this.http._get(`admin/drugs/all`);
  }

  getDrugDetail (drugId) {
    return this.http._get(`admin/drugs/${drugId}`);
  }

  saveDrug (drug) {
    return this.http._post(`admin/drugs/save`, drug);
  }

  removeDrug (drugId) {
    return this.http._post(`admin/drugs/${drugId}`, {});
  }
}
