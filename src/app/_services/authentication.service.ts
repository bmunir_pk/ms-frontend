import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from './../_models/user';
import {ConfigService} from './config.service';
import {Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private configService: ConfigService, private router: Router ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('_user_')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email, password) {
    return this.http.post<any>(`${this.configService.baseUrl}login`, { email, password })
      .pipe(map(res => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        this.setUserInfo(res.user);
        localStorage.setItem('_token', res.token);
        this.currentUserSubject.next(res.user);
        return res.user;
      }));
  }

  setUserInfo (user) {
    localStorage.setItem('_user_', JSON.stringify(user));
  }

  getToken() {
    return localStorage.getItem('_token');
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('_user_');
    localStorage.removeItem('_token');
    this.currentUserSubject.next(null);
  }

  verifyToken() {
    return this.http.post<any>(`${this.configService.baseUrl}verify-token`, { token: this.getToken(), email: this.currentUserValue.email })
      .subscribe(
        res => {

        },
        error => {
          console.log(error)
          if(error.error.error) {
            this.logout();
          }
        });
  }

}
