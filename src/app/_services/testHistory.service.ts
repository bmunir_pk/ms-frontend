import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from './config.service';
import {HttpService} from './http.service';

@Injectable({ providedIn: 'root' })
export class TestHistoryService {
  constructor(private http: HttpService) { }

  getTestHistory(patientId=0) {
    if(patientId == 0) {
      return this.http._get(`history/covid-test`);
    }
    else {
      return this.http._get(`history/${patientId}/covid-test`);
    }
  }

}
