import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ConfigService {
  baseUrl = '/index.php/api/';
  //baseUrl = 'http://covid-19-api.local/index.php/api/';
}
