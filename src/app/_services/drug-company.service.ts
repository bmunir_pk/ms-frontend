import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ConfigService} from './config.service';
import {HttpService} from './http.service';

@Injectable({ providedIn: 'root' })
export class DrugCompanyService {
  companyForm= {
      title: "",
      description: "",
      logo: "",
      website_url: "",
      phone_number: "",
      address: ""
  }

  constructor(private http: HttpService) { }

  getAllCompanies() {
    return this.http._get(`admin/companies/all`);
  }

  getCompanyDetail (companyId) {
    return this.http._get(`admin/companies/${companyId}`);
  }

  saveCompany (company) {
    return this.http._post(`admin/companies/save`, company);
  }

  removeCompany (companyId) {
    return this.http._post(`admin/companies/${companyId}`, {});
  }

}
