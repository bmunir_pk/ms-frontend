import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {shareReplay} from "rxjs/operators";
import {ConfigService} from './config.service';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(
    private http: HttpClient,
    private appConfigService: ConfigService,
    private authService: AuthenticationService
    ) {}

  public _get(url, params= {}) {
    return this.http.get(this.appConfigService.baseUrl + url, {
      params,
      headers: this.getHeaders()
    });
    //  .pipe(shareReplay({ bufferSize: 1, refCount: true }));
  }

  public _post(url, body, params= {}) {
    return this.http.post(this.appConfigService.baseUrl + url, body, {
      params,
      headers: this.getHeaders()
    });
    //  .pipe(shareReplay({ bufferSize: 1, refCount: true }));
  }

  public _delete(url, params= {}){
    return this.http.delete(this.appConfigService.baseUrl + url, {
      params,
      headers: this.getHeaders()
    });
  }

  public _patch(url, body= {}, params= {}){
    return this.http.patch(this.appConfigService.baseUrl + url, body, {
      params,
      headers: this.getHeaders()
    });
  }

  private getHeaders() {
    const headers = new HttpHeaders().set('Authorization',  'Bearer ' + this.authService.getToken())
      .set('Accept', 'application/json');
      //.set('Access-Control-Allow-Origin', );
    return headers;
  }
}
