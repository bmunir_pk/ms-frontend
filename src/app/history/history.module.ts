import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {routing} from './history.routing';
import {MedicalHistoryComponent} from './medical-history/medical-history.component';
import {TestHistoryComponent} from './test-history/test-history.component';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    MedicalHistoryComponent,
    TestHistoryComponent
  ],
  imports: [
    CommonModule,
    routing,
    NgbTooltipModule
  ]
})
export class HistoryModule { }
