import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'
import {TestHistoryComponent} from './test-history/test-history.component';
import {MedicalHistoryComponent} from './medical-history/medical-history.component';

export const routes: Routes = [
  { path: '', component: TestHistoryComponent },//patient/doctor's history of usage like last 5 tests he conducted
  { path: 'tests', component: TestHistoryComponent },//patient/doctor's history of usage like last 5 tests he conducted
  { path: 'tests/:patient_id/patient', component: TestHistoryComponent },
  { path: 'medication', component: MedicalHistoryComponent },
  { path: 'medication/:patient_id/patient', component: MedicalHistoryComponent },
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes)
