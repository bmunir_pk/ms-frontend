import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {TestHistoryService} from '../../_services/testHistory.service';
import {PatientService} from '../../_services/patient.service';
import {ActivatedRoute} from '@angular/router';
import {DateParser} from '../../_helpers/DateParser';

@Component({
  selector: 'app-test-history',
  templateUrl: './test-history.component.html',
  styleUrls: ['./test-history.component.css']
})
export class TestHistoryComponent implements OnInit {
  state= {
    processing: false
  }
  tests = [];
  parseDate;
  constructor(public authService: AuthenticationService,
              public testService: TestHistoryService,
              private activatedRoute: ActivatedRoute,
              ) {
    this.parseDate= new DateParser();
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.loadHistory(params['patient_id'] || 0);
    });
  }

  loadHistory (patientId = 0) {
    if(this.state.processing) return;
    this.state.processing = true;
    this.testService.getTestHistory(patientId)
      .subscribe(
        res => {
          this.tests = res['data'];
          this.state.processing = false;
        },
        err => { this.state.processing = false; },
      ()=>{this.state.processing = false;}
      )
  }

}
