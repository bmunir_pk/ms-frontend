import { Component, OnInit } from '@angular/core';
import {HttpService} from '../_services/http.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {

  constructor(private httpService: HttpService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }
  private code;
  private email;
  state = { processing: false, message: '', isVerified: false};
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {

      this.code = params["c"];
      this.email = params["a"];
      this.state.processing = true;//
      //`verify-email/${this.email}/${this.code}`
      this.httpService._get('verify-email/'+this.email+"/"+this.code)
        .subscribe(
          res => {
            console.log(res)
            this.state.processing=false;
            this.state.message = res['message'];
            this.state.isVerified= true
          },
          err => {
            this.state.processing=false;
            this.state.message = err.error.message;
          }
        )
    });

  }

}
