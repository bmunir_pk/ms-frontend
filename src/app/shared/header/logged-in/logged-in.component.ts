import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../_services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.css']
})
export class LoggedInComponent implements OnInit {
  @Input() currentUser;
  constructor( private authService: AuthenticationService,
               private router: Router) { }

  ngOnInit(): void {
  }

  logout () {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}
