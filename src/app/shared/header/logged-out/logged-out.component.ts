import {Component, Input, OnInit} from '@angular/core';
import {HttpService} from '../../../_services/http.service';
import {AuthenticationService} from '../../../_services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-logged-out',
  templateUrl: './logged-out.component.html',
  styleUrls: ['./logged-out.component.css']
})
export class LoggedOutComponent implements OnInit {
  @Input() currentUser;

  public form = {
    email: '',
    password : ''
  };
  state = {
    isProcessing: false,
    errors: {}
  }
  returnUrl: string;
  constructor(private http: HttpService,
              private authService: AuthenticationService,
              private route: ActivatedRoute,
              private router: Router,
              private _flashMessagesService: FlashMessagesService,
              private toastr: ToastrService) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
  }

  login() {
    this.state.isProcessing = true;
    this.state.errors = {}
    this.authService.login(this.form.email, this.form.password)
      .subscribe(
        data => {
          if (this.authService.currentUserValue.type == 1) { //check if admin then navigate to admin url
            this.router.navigate(['/admin']);
          }
          this.router.navigate([this.returnUrl|| '/']);
        },
        error => {
          console.log(error.error)
          this.toastr.error(error.error.message, "Error")
          this.state.isProcessing = false;
        });
  }
}
