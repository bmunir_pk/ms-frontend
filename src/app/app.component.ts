import { Component } from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {AuthenticationService} from './_services/authentication.service';
import {User} from './_models/user';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  currentUser: User;
  constructor(public router: Router,
              private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    router.events.subscribe((val) => {
      // see also
      if(val instanceof NavigationEnd){
        //jQuery(".")
        //console.log(router.url)
        if (this.authenticationService.currentUserValue.type == 0 && router.url.indexOf('admin') !== -1) { //check if admin then navigate to admin url
          this.router.navigate(['/']);
        }
        if (this.authenticationService.currentUserValue.type == 1 && router.url.indexOf('admin') == -1) { //check if admin then navigate to admin url
          this.router.navigate(['/admin']);
        }
        document.getElementById('left-navigation').click();
      }
    });
    if(this.authenticationService.currentUserValue){
      this.authenticationService.verifyToken();
    }
  }
}
